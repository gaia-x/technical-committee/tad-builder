#!/usr/bin/env python3
import os
import requests
import coloredlogs, logging
from atlassian import Confluence
from collections import namedtuple
from jinja2 import Environment, FileSystemLoader, select_autoescape


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
logging.getLogger("urllib3.connectionpool").setLevel(logging.INFO)
logging.getLogger("atlassian.rest_client").setLevel(logging.INFO)

fmt='%(asctime)s,%(msecs)03d %(hostname)s %(name)s[%(process)d] %(levelname)s %(message)s'
fmt='%(name)s[%(process)d] %(levelname)s %(message)s'
coloredlogs.install(level='DEBUG', logger=logger, fmt=fmt)

confluence_host = os.environ['CONFLUENCE_HOST']
confluence_username = os.environ['CONFLUENCE_USERNAME']
confluence_token = os.environ['CONFLUENCE_TOKEN']
root_page = 442400835

Contributor = namedtuple('Contributor', ['name', 'email'])
Section = namedtuple('Section', ['title', 'page_id', 'last_modification', 'content', 'edit_url'])

def create_section(page):
    title = page['title']
    last_modification = page['version']['when']
    content = page['body']['view']['value']
    edit_url = page['_links']['webui']
    return Section(title=title, last_modification=last_modification, edit_url=edit_url, page_id=page['id'], content=content)


if __name__ == '__main__':
    confluence = Confluence(url=confluence_host, username=confluence_username, password=confluence_token)
    tad = confluence.get_page_child_by_type(root_page, type='page', start=None, limit=None)

    sections = []
    contributors = set()

    page = confluence.get_page_by_id(root_page, expand='version,body.view', status=None, version=None)
    sections.append(create_section(page))

    for page in tad:
        # labels = confluence.get_page_labels(page['id'], prefix=None, start=None, limit=None)
        # if 'tad_content' not in map(lambda e: e['name'], labels['results']):
        #     continue
        logger.info(page['title'])
        page = confluence.get_page_by_id(page['id'], expand='history.contributors.publishers.users,version,body.view', status=None, version=None)
        section = create_section(page)
        sections.append(section)
        contributors.update(list(map(lambda e: Contributor(name=e['publicName'], email=e['email']), page['history']['contributors']['publishers']['users'])))

    env = Environment(
        loader=FileSystemLoader('templates'),
        autoescape=select_autoescape(['html', 'xml'])
    )
    tad = env.get_template('index.html.j2')
    logger.debug(tad.filename)
    with open('index.html', 'w') as fd:
        fd.write(tad.render({'sections': sections, 'root_url': confluence_host, 'contributors': contributors}))
        fd.close()
